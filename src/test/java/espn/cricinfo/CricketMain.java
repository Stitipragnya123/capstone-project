package espn.cricinfo;

public class CricketMain {

	public static void main(String[] args) {
 		MatchInitializer match=new  MatchInitializer();
 		match.initDriver();
 		match.navigateEspncricinfo();
 		match.maximizeWindow();
 		match.waitToLoad();
 		CricketHome cric=new CricketHome();
 		cric.clickOnElements();
 		CricketScore score=new CricketScore();
 		score.getMatchLinks();
 		score.clickMatchLinks();

	}

}
