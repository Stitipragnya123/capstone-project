package espn.cricinfo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CricketHome extends MatchInitializer{
	private WebElement popup = driver.findElement(By.xpath("//div/button[@id='wzrk-cancel' and @class='No thanks']"));
	private WebElement liveScores = driver.findElement(By.xpath("//*[@id='navbarSupportedContent']/ul[1]/li[1]/a"));

	public void clickOnElements() {
		popup.click();
		liveScores.click();
	}


}
